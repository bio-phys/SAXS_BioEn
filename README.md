# SAXS_BioEn

Using a simple example, the Jupyter notebook SAXS\_BioEn\_example.ipynb shows how
to refine structural ensembles with BioEn (https://github.com/bio-phys/bioen)
and using SAXS data. At the end of the notebook and as an option, we evaluate
the h-statistic and (h,$\chi^2$)-statistic provided by the hplusminus software (https://github.com/bio-phys/hplusminus) to validate
the goodness-of-refinement. 

The Jupyter notebook runs using Python 3 and the current versions of the bioen and hplusminus software. 
